package com.example.avtcompose.database

import androidx.lifecycle.LiveData
import com.example.avtcompose.model.Task

interface ITasksLocalRepository {
    fun getAll(): LiveData<List<Task>>
    suspend fun findById(id: Long): Task?
    suspend fun insertTask(task: Task): Long
    suspend fun updateTask(task: Task)
    suspend fun deleteTask(task: Task)
    suspend fun deleteAllTasks()
}