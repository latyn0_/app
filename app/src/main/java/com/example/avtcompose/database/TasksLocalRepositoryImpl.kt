package com.example.avtcompose.database

import androidx.lifecycle.LiveData
import com.example.avtcompose.model.Task

class TasksLocalRepositoryImpl(private val dao: TasksDao) : ITasksLocalRepository {
    override fun getAll(): LiveData<List<Task>> {
        return dao.getAll()
    }

    override suspend fun findById(id: Long): Task? {
        return dao.findById(id)
    }

    override suspend fun insertTask(task: Task): Long {
        return dao.insertTask(task)
    }

    override suspend fun updateTask(task: Task) {
        return dao.updateTask(task)
    }

    override suspend fun deleteTask(task: Task) {
        return dao.deleteTask(task)
    }

    override suspend fun deleteAllTasks() {
        return dao.deleteAllTasks()
    }
}