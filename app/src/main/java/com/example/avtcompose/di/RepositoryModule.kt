package com.example.avtcompose.di

import com.example.avtcompose.database.ITasksLocalRepository
import com.example.avtcompose.database.TasksDao
import com.example.avtcompose.database.TasksLocalRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {
    single { provideLocalTasksRepository(get()) }

}

fun provideLocalTasksRepository(dao: TasksDao): ITasksLocalRepository =
    TasksLocalRepositoryImpl(dao)