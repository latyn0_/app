package com.example.avtcompose.di

import com.example.avtcompose.ui.TaskDetailViewModel
import com.example.avtcompose.ui.TaskListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel {
        TaskListViewModel(get())
    }

    viewModel {
        TaskDetailViewModel(get())
    }
}