package com.example.avtcompose.di

import android.content.Context
import com.example.avtcompose.database.TasksDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val databaseModule = module {
    single {
        provideDatabase(androidContext())
    }

}

fun provideDatabase(context: Context): TasksDatabase =
    TasksDatabase.getInstance(context)