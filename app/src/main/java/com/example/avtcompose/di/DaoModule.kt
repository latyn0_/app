package com.example.avtcompose.di

import com.example.avtcompose.database.TasksDao
import com.example.avtcompose.database.TasksDatabase
import org.koin.dsl.module

val daoModule = module {
    single {
        provideTasksDao(get())
    }

}

fun provideTasksDao(database: TasksDatabase): TasksDao =
    database.tasksDao()