@file:Suppress("JoinDeclarationAndAssignment")

package com.example.avtcompose.utils

import java.text.SimpleDateFormat
import java.util.*

class DateUtils {
    companion object {
        private const val DATE_FORMAT_CS = "dd. MM. yyyy"
        private const val DATE_FORMAT_EN = "yyyy/MM/dd"


        fun getDateString(unixTime: Long): String{
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = unixTime
            val format: SimpleDateFormat
            format = if (LanguageUtils.isLanguageCzech()){
                SimpleDateFormat(DATE_FORMAT_CS, Locale.GERMAN)
            } else {
                SimpleDateFormat(DATE_FORMAT_EN, Locale.ENGLISH)
            }
            return format.format(calendar.time)
        }


        fun getUnixTime(year: Int, month: Int, day: Int): Long {
            val calendar = Calendar.getInstance()
            calendar.set(year, month, day)
            return calendar.timeInMillis
        }
    }
}