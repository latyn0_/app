package com.example.avtcompose.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "tasks")
data class Task(@ColumnInfo(name = "text") var text: String, var desc: String?) {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long? = null

    @ColumnInfo(name = "description")
    var description: String? = desc

    @ColumnInfo(name = "date")
    var date: Long? = null

    @ColumnInfo(name = "done")
    var done: Boolean = false

    @ColumnInfo(name = "latitude")
    var latitude: Double? = null

    @ColumnInfo(name = "longitude")
    var longitude: Double? = null

}