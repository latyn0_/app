package com.example.avtcompose.ui

import android.app.DatePickerDialog
import android.content.Context
import android.widget.DatePicker
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.avtcompose.R
import com.example.avtcompose.utils.DateUtils
import kotlinx.coroutines.launch
import org.koin.androidx.compose.getViewModel
import java.util.*

@ExperimentalMaterial3Api
@Composable
fun AddTask(navController: NavController, id: Long?) {
    val viewModel = getViewModel<TaskDetailViewModel>()
    val snackbarHostState = remember { SnackbarHostState() }
    val scope = rememberCoroutineScope()
    var taskName by rememberSaveable { mutableStateOf("") }
    var taskDesc by rememberSaveable { mutableStateOf("") }
    var taskDate = remember { mutableStateOf("") }
    val taskLocation = remember { mutableStateOf("") }
    val context = LocalContext.current

    if (id != -1L) {
        viewModel.getTask(id!!)
        taskName = viewModel.task!!.text
        taskDesc = viewModel.task!!.description ?: ""
        if (viewModel.task?.date != null) {
            taskDate = remember {
                mutableStateOf(DateUtils.getDateString(viewModel.task?.date!!))
            }
        }
        viewModel.updating = true
    }

    Scaffold(
        snackbarHost = { SnackbarHost(hostState = snackbarHostState) },
        topBar = {
            AddTaskTopAppBar(navController = navController)
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(10.dp)
        ) {

            OutlinedTextField(
                value = taskName,
                onValueChange = {
                    taskName = it
                    viewModel.task?.text = it
                },
                label = { Text("Task name") },
                singleLine = true,
                modifier = Modifier
                    .width(300.dp)
            )

            OutlinedTextField(
                value = taskDesc,
                onValueChange = {
                    taskDesc = it
                    viewModel.task?.description = it
                },
                label = { Text("Description") },
                singleLine = true,
                modifier = Modifier
                    .width(300.dp)
            )

            DatePicker(context = context, date = taskDate)

            MapPicker(navController = navController, location = taskLocation)

            Button(onClick = {
                if (taskName == "") {
                    scope.launch {
                        snackbarHostState.showSnackbar("Invalid input")
                    }
                } else {
                    viewModel.task?.text = taskName
                    viewModel.task?.description = taskDesc
                    viewModel.insertTask()
                    navController.popBackStack()
                }

            }) {
                Text(text = "Save Task")
            }
        }
    }
}


@Composable
fun MapPicker(navController: NavController, location: MutableState<String>) {
    val interactionSource = remember { MutableInteractionSource() }
    val isPressed: Boolean by interactionSource.collectIsPressedAsState()

    if (isPressed) {
        navController.navigate("map")
    }

    OutlinedTextField(
        value = location.value,
        onValueChange = { location.value = it },
        interactionSource = interactionSource,
        label = { Text("Location") },
        leadingIcon = {
            Icon(
                painter = painterResource(id = R.drawable.ic_place),
                contentDescription = "Location"
            )
        },
        trailingIcon = {
            if (location.value != "") {
                IconButton(onClick = { location.value = "" }) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_clear),
                        contentDescription = "clear"
                    )
                }
            }
        },
        readOnly = true,
        modifier = Modifier
            .width(300.dp)

    )
}


@Composable
fun DatePicker(context: Context, date: MutableState<String>) {
    val interactionSource = remember { MutableInteractionSource() }
    val isPressed: Boolean by interactionSource.collectIsPressedAsState()
    val viewModel = getViewModel<TaskDetailViewModel>()
    val year: Int
    val month: Int
    val day: Int

    val calendar = Calendar.getInstance()
    year = calendar.get(Calendar.YEAR)
    month = calendar.get(Calendar.MONTH)
    day = calendar.get(Calendar.DAY_OF_MONTH)
    calendar.time = Date()

    val datePickerDialog = DatePickerDialog(
        context,
        { _: DatePicker, year: Int, month: Int, dayOfMonth: Int ->
            date.value = "$dayOfMonth/$month/$year"
            viewModel.task?.date = DateUtils.getUnixTime(year, month, dayOfMonth)
        }, year, month, day
    )

    if (isPressed) {
        datePickerDialog.show()
    }

    OutlinedTextField(
        value = date.value,
        onValueChange = {
            date.value = it
        },
        interactionSource = interactionSource,
        label = { Text("Date") },
        leadingIcon = {
            Icon(
                painter = painterResource(id = R.drawable.ic_calendar_today),
                contentDescription = "calendar"
            )
        },
        trailingIcon = {
            if (date.value != "") {
                IconButton(onClick = {
                    date.value = ""
                    viewModel.task?.date = null
                }) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_clear),
                        contentDescription = "clear"
                    )
                }
            }
        },
        readOnly = true,
        modifier = Modifier
            .width(300.dp)

    )
}