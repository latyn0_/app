package com.example.avtcompose.ui

import android.os.Bundle
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.navigation.NavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

@ExperimentalMaterial3Api
@Composable
fun MapView(navController: NavController) {
    val position = LatLng(49.209668339829136, 16.614964626207776)

    Scaffold(
        topBar = {
            MapTopAppBar(navController = navController)
        }
    ) {
        Box(
            modifier = Modifier.fillMaxSize()
        ) {
            MapPicker(
                modifier = Modifier
                    .fillMaxSize(),
                position = position,
                zoom = 16f
            ) {

            }
        }
    }
}

@Composable
fun MapPicker(
    modifier: Modifier = Modifier,
    position: LatLng,
    zoom: Float,
    onReady: (GoogleMap) -> Unit,
) {

    val context = LocalContext.current

    val mapView = remember {
        MapView(context)
    }

    val lifecycle = LocalLifecycleOwner.current.lifecycle

    lifecycle.addObserver(rememberMapLifecycle(map = mapView))

    AndroidView(
        factory = {
            mapView.apply {
                mapView.getMapAsync { googleMap ->
                    googleMap.addMarker(MarkerOptions().position(position))
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, zoom))
                    onReady(googleMap)
                }
            }
        },
        modifier = modifier
    )

}

@Composable
fun rememberMapLifecycle(map: MapView): LifecycleEventObserver {
    return remember {
        LifecycleEventObserver { source, event ->
            when (event) {
                Lifecycle.Event.ON_CREATE -> map.onCreate(Bundle())
                Lifecycle.Event.ON_START -> map.onStart()
                Lifecycle.Event.ON_RESUME -> map.onResume()
                Lifecycle.Event.ON_PAUSE -> map.onPause()
                Lifecycle.Event.ON_STOP -> map.onPause()
                Lifecycle.Event.ON_DESTROY -> map.onDestroy()
                Lifecycle.Event.ON_ANY -> throw IllegalStateException()
            }
        }
    }
}