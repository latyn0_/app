package com.example.avtcompose.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.avtcompose.database.ITasksLocalRepository
import com.example.avtcompose.model.Task
import kotlinx.coroutines.launch

class TaskListViewModel(private val repository: ITasksLocalRepository) : ViewModel() {
    fun getAll(): LiveData<List<Task>> {
        return repository.getAll()
    }

    fun deleteTask(task: Task) {
        viewModelScope.launch {
            repository.deleteTask(task)
        }
    }

    fun updateTask(task: Task) {
        viewModelScope.launch {
            repository.updateTask(task)
        }
    }
}