package com.example.avtcompose.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.avtcompose.model.Task
import org.koin.androidx.compose.getViewModel

@ExperimentalMaterial3Api
@Composable
fun Home(navController: NavController) {
    val viewModel = getViewModel<TaskListViewModel>()
    val snackbarHostState = remember { SnackbarHostState() }
    val list = viewModel.getAll().observeAsState(arrayListOf())

    Scaffold(
        snackbarHost = { SnackbarHost(hostState = snackbarHostState) },
        topBar = {
            TasksListTopAppBar()
        },
        floatingActionButton = {
            FloatingActionButton(
                onClick = { navController.navigate("addTask?id={id}") },
            ) {
                Icon(Icons.Filled.Add, "Add Button")
            }
        }
    ) {
        Column {
            List(list, navController)
        }
    }
}

@ExperimentalMaterial3Api
@Composable
fun List(list: State<List<Task>>, navController: NavController) {
    LazyColumn(content = {
        items(
            items = list.value,
            itemContent = {
                Row(it, navController)
            }
        )
    }
    )
}

@ExperimentalMaterial3Api
@Composable
fun Row(task: Task, navController: NavController) {
    val isChecked = remember { mutableStateOf(task.done) }
    val viewModel = getViewModel<TaskListViewModel>()

    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .clickable {
                navController.navigate("addTask?id=${task.id}")
            }
    ) {
        Checkbox(
            checked = isChecked.value,
            onCheckedChange = {
                isChecked.value = it
                task.done = it
                viewModel.updateTask(task)
            },
            enabled = true
        )

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)

        ) {
            Text(
                text = task.text,
                modifier = Modifier
                    .padding(10.dp),
                maxLines = 2,
                overflow = TextOverflow.Ellipsis

            )
            if (task.description != "") {
                Text(
                    text = task.description!!,
                    modifier = Modifier
                        .padding(10.dp),
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis
                )
            }
        }
        Column(
            modifier = Modifier
        ) {
            IconButton(
                onClick = {
                    viewModel.deleteTask(task)
                },
            ) {
                Icon(imageVector = Icons.Filled.Delete, contentDescription = "delete task")
            }
        }
    }
}

