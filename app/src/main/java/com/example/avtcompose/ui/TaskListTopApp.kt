package com.example.avtcompose.ui

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.SmallTopAppBar
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.navigation.NavController

@Composable
fun TasksListTopAppBar() {
    SmallTopAppBar(
        title = { Text("Tasks List") },
        //actions = { TopAppBarDropdownMenu(mTasksViewModel = mTasksViewModel) }
    )
}

@Composable
fun MapTopAppBar(navController: NavController) {
    SmallTopAppBar(
        title = { Text("Location") },
        navigationIcon = {
            IconButton(onClick = { navController.popBackStack() }) {
                Icon(
                    imageVector = Icons.Filled.ArrowBack,
                    contentDescription = "Go Back Button"
                )
            }
        },
        actions = {
            IconButton(
                onClick = { navController.popBackStack() }
            ) {
                Text(text = "SAVE")
            }
        }
    )
}

@Composable
fun AddTaskTopAppBar(navController: NavController) {
    SmallTopAppBar(
        title = { Text("Add Task") },
        navigationIcon = {
            IconButton(onClick = { navController.popBackStack() }) {
                Icon(
                    imageVector = Icons.Filled.ArrowBack,
                    contentDescription = "Go Back Button"
                )
            }
        }
    )
}

