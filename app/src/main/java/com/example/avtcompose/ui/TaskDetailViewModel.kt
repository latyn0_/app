package com.example.avtcompose.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.avtcompose.database.ITasksLocalRepository
import com.example.avtcompose.model.Task
import kotlinx.coroutines.launch

class TaskDetailViewModel(private val repository: ITasksLocalRepository) : ViewModel() {

    var task: Task? = Task("", "")

    var updating = false

    fun insertTask() {
        viewModelScope.launch {
            if (updating) {
                repository.updateTask(task!!)
            } else {
                repository.insertTask(task!!)
            }
        }
    }

    fun getTask(id: Long) {
        viewModelScope.launch {
            task = repository.findById(id)
        }
    }
}