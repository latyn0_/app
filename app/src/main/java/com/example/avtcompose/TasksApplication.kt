package com.example.avtcompose

import android.app.Application
import com.example.avtcompose.di.daoModule
import com.example.avtcompose.di.databaseModule
import com.example.avtcompose.di.repositoryModule
import com.example.avtcompose.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class TasksApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(applicationContext)
            modules(
                databaseModule,
                daoModule,
                repositoryModule,
                viewModelModule
            )
        }
    }
}